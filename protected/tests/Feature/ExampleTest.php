<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\PassportTestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use DB;

class ExampleTest extends PassportTestCase
{
    protected $scopes = ['restricted-scope'];

    public function testBasicTest()
    {
        // $response = $this->get('/');

        // $response->assertStatus(200);
        $response = $this->json('POST', 'api/apply', [
            "subject"       => "hello",
            "proposal"      => "This is a proposal",
            "budget"        => 10000,
            "date_estimate" => "2018-08-01",
            "id_job"        => 1
        ]);
        $response
            ->assertStatus(201)
            ->assertExactJson([
                'created' => true,
            ]);
    }
}
