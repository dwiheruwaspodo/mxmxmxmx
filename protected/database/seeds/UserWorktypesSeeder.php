<?php

use Illuminate\Database\Seeder;

class UserWorktypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('user_worktypes')->delete();
        
        \DB::table('user_worktypes')->insert([
    		[
				'id_customer_worktype' => 1,
				'id_user'              => 2,
				'id_worktype'          => 1,
				'created_at'           => date('Y-m-d H:i:s'),
				'updated_at'           => date('Y-m-d H:i:s'),
    		]
        ]);
    }
}
