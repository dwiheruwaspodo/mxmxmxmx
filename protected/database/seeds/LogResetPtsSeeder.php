<?php

use Illuminate\Database\Seeder;

class LogResetPtsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('log_reset_pts')->delete();
        
        \DB::table('log_reset_pts')->insert([
    		[
				'id_log_reset_pts' => 1,
				'id_user'          => 2,
				'reset_at'         => date('Y-m-d H:i:s'),
				'created_at'       => date('Y-m-d H:i:s'),
				'updated_at'       => date('Y-m-d H:i:s'),
            ],
        ]);
    }
}
