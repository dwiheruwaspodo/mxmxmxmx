<?php

use Illuminate\Database\Seeder;

class JobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('jobs')->delete();
        
        \DB::table('jobs')->insert([
    		[
				'id_jobs'     => 1,
				'jobs'       => 'Programmer',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
				'id_jobs'     => 2,
				'jobs'       => 'QA',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
				'id_jobs'     => 3,
				'jobs'       => 'UI/UX',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s'),
            ]
        ]);
    }
}
