<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->delete();
        
        \DB::table('users')->insert([
    		[
                'id'         => 1,
                'name'       => 'Dwi',
                'email'      => 'dwi@mail.com',
                'level'      => 'employer',
                'pts'        => 0,
                'password'   => Hash::make('123123'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
				'id'         => 2,
				'name'       => 'Heru',
				'email'      => 'heru@mail.com',
				'level'      => 'worker',
				'pts'        => 40,
				'password'   => Hash::make('123123'),
				'created_at' => date('Y-m-d H:i:s'),
    			'updated_at' => date('Y-m-d H:i:s'),
    		]
        ]);
    }
}
