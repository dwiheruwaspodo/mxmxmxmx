<?php

use Illuminate\Database\Seeder;

class OauthTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('oauth_clients')->delete();
        
        \DB::table('oauth_clients')->insert([
    		[
				'id'                     => 1,
				'user_id'                => null,
				'name'                   => "Laravel Personal Access Client",
				'secret'                 => "yOt4btX96mh3I8YAJz11Gy2wSZbP9UdCjcxamz7l",
				'redirect'               => "http://localhost",
				'personal_access_client' => 1,
				'password_client'        => 0,
				'revoked'                => 0,
				'created_at'             => date('Y-m-d H:i:s'),
				'updated_at'             => date('Y-m-d H:i:s'),
    		],
    		[
				'id'                     => 2,
				'user_id'                => null,
				'name'                   => "Laravel Password Grant Client",
				'secret'                 => "26Kd6VD8rKJTbU684xLLTwQJJDB4eZy4G7ie7hyj",
				'redirect'               => "http://localhost",
				'personal_access_client' => 0,
				'password_client'        => 1,
				'revoked'                => 0,
				'created_at'             => date('Y-m-d H:i:s'),
				'updated_at'             => date('Y-m-d H:i:s'),
    		]
        ]);
    }
}
