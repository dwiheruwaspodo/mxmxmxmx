<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(OauthTable::class);
        $this->call(UsersSeeder::class);
        $this->call(WorktypesSeeder::class);
        $this->call(UserWorktypesSeeder::class);
        $this->call(LogResetPtsSeeder::class);
        $this->call(JobSeeder::class);
    }
}
