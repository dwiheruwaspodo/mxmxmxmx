<?php

use Illuminate\Database\Seeder;

class WorktypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('worktypes')->delete();
        
        \DB::table('worktypes')->insert([
    		[
				'id_worktype' => 1,
				'type'        => "A",
				'max_upload'  => 20,
				'start_point' => 40,
				'created_at'  => date('Y-m-d H:i:s'),
				'updated_at'  => date('Y-m-d H:i:s'),
    		],
    		[
				'id_worktype' => 2,
				'type'        => "B",
				'max_upload'  => 10,
				'start_point' => 20,
				'created_at'  => date('Y-m-d H:i:s'),
				'updated_at'  => date('Y-m-d H:i:s'),
    		]
        ]);
    }
}
