<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerWorkTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_worktypes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_customer_worktype');
            $table->unsignedInteger('id_user');
            $table->foreign('id_user', 'fk_users_idx')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->unsignedInteger('id_worktype');
            $table->foreign('id_worktype', 'fk_worktypes_idx')
                ->references('id_worktype')->on('worktypes')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_worktypes');
    }
}
