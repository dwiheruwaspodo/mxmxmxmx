<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLastResetApplyProposal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('proposal', function(Blueprint $table) {
            $table->unsignedInteger('id_last_reset')->after('pts_after');
            $table->foreign('id_last_reset', 'fk_last_reset_idx')
                ->references('id_log_reset_pts')->on('log_reset_pts')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proposal', function(Blueprint $table) {
            $table->dropColumn('id_last_reset');
        });
    }
}
