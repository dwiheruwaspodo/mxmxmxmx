<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProposal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposal', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_proposal');
            $table->unsignedInteger('id_user');
            $table->foreign('id_user', 'fk_user_proposal_idx')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->string('subject', 30);
            $table->text('proposal');
            $table->integer('budget')->default(0);
            $table->date('date_estimate')->nullable();
            $table->integer('pts_before')->default(0);
            $table->integer('pts_after')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proposal');
    }
}
