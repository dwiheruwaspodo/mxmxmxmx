<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJobOnProposalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('proposal', function(Blueprint $table) {
            $table->unsignedInteger('id_job')->after('pts_after')->nullable();
            $table->foreign('id_job', 'fk_job_idx')
                ->references('id_jobs')->on('jobs')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proposal', function(Blueprint $table) {
            $table->dropColumn('id_job');
        });
    }
}
