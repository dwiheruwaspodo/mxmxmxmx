<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Apply extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_job'        => 'required|integer',
            'subject'       => 'required|max:30',
            'proposal'      => 'required|max:10000',
            'budget'        => 'required|integer',
            'date_estimate' => 'required|date|date_format:"Y-m-d"'
        ];
    }
}
