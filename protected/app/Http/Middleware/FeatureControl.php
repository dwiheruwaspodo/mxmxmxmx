<?php

namespace App\Http\Middleware;

use Closure;

class FeatureControl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $feature)
    {
        if ($request->user()->level == $feature) {
            return $next($request);
        }

        return response()->json(['status' => 'fail', 'messages' => 'You do not have access']);
    }
}
