<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

// MODEL
use App\Http\Models\Proposal;

// REQUEST
use App\Http\Requests\Apply;

class ProposalController extends Controller
{
    public function __construct()
    {
    	date_default_timezone_set('Asia/Jakarta');
    }

    /* LIST PROPOSAL */
    function listProposal(Request $request)
    {
		$post = $request->json()->all();	

		return response()->json(parent::returnResult(Proposal::listProposal($post)));
    }
}
