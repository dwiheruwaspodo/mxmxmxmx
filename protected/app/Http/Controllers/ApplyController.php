<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

// MODEL
use App\Http\Models\Proposal;
use App\Http\Models\User;
use App\Http\Models\UserWorkType;
use App\Http\Models\LogResetPts;

// REQUEST
use App\Http\Requests\Apply;

class ApplyController extends Controller
{
    public function __construct()
    {
    	date_default_timezone_set('Asia/Jakarta');
    }

    /* APPLY JOB */
    function apply(Apply $request)
    {
    	DB::beginTransaction();
    	$user = User::userInfo($request->user()->id);

    	if ($allow = $this->allow($user, $request->json('id_job'))) {
			$data['id_job']        = $request->json('id_job');
			$data['subject']       = $request->json('subject');
			$data['proposal']      = $request->json('proposal');
			$data['budget']        = $request->json('budget');
			$data['date_estimate'] = $request->json('date_estimate');
			$data['id_user']       = $user[0]['id'];
			$data['pts_before']    = $user[0]['pts'];
			$data['pts_after']     = $user[0]['pts'] - 2;
			$data['id_last_reset'] = $allow;

			// CHECK IF PTS AFTER < 0
			if ($data['pts_after'] >= 0) {
				// SAVE PROPOSAL
				if (Proposal::create($data)) {
					// UPDATE PTS USER
					if (User::where('id', $user[0]['id'])->update(['pts' => $data['pts_after']])) {
						DB::commit();
						return response()->json(parent::returnExe(true));
					}
				}
			}
    	}

    	DB::rollback();
    	return response()->json(parent::returnExe(false));
    }

    /* CHECK ALLOW TO APPLY */
    function allow($user, $job)
    {
    	// CHECK LAST RESET
    	$lastReset = LogResetPts::lastReset($user[0]['id']);

    	if ($lastReset) {
    		// CHECK ALLOW TO APPLY
    		if (Proposal::countProposal($lastReset) <= $user[0]['user_worktypes'][0]['worktype']['max_upload'] && Proposal::uniqueJob($user[0]['id'], $job) == false) {

    			return $lastReset;
    		}
    	}
    	
    	return false;
    }
}
