<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Worktype extends Model
{
    protected $table = 'worktypes';

    protected $primaryKey = 'id_worktype';

    protected $fillable = ['type', 'max_upload', 'start_point', 'created_at', 'updated_at'];

    public function userWorktypes()
    {
        return $this->hasMany('App\Http\Models\UserWorktype', 'id_worktype', 'id_worktype');
    }
}
