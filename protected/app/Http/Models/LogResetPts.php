<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class LogResetPts extends Model
{
    
    protected $table = 'log_reset_pts';

    protected $primaryKey = 'id_log_reset_pts';

    protected $fillable = ['id_user', 'reset_at', 'created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo('App\Http\Models\User', 'id_user');
    }

    /* LAST RESET */
    static function lastReset($idUser)
    {
        $last = LogResetPts::where('id_user', $idUser)->orderBy('reset_at', 'DESC')->first();

        if ($last) {
            $last = $last->id_log_reset_pts;
        }

        return $last;
    }


}
