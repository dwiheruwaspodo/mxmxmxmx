<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $table = 'users';

    protected $fillable = ['name', 'email', 'password', 'remember_token', 'level', 'pts', 'created_at', 'updated_at'];

    public function logResetPts()
    {
        return $this->hasMany('App\Http\Models\LogResetPt', 'id_user');
    }

    public function proposals()
    {
        return $this->hasMany('App\Http\Models\Proposal', 'id_user');
    }

    public function userWorktypes()
    {
        return $this->hasMany('App\Http\Models\UserWorktype', 'id_user');
    }

    /* INFO USER */
    static function userInfo($id)
    {
        return Self::with(['userWorktypes', 'userWorktypes.worktype'])->where('id', $id)->get()->toArray();
    }

}
