<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class UserWorkType extends Model
{
    protected $table = 'user_worktypes';

    protected $primaryKey = 'id_customer_worktype';

    protected $fillable = ['id_user', 'id_worktype', 'created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo('App\Http\Models\User', 'id_user');
    }

    public function worktype()
    {
        return $this->belongsTo('App\Http\Models\Worktype', 'id_worktype', 'id_worktype');
    }
}
