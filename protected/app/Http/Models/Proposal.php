<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Proposal extends Model
{
    protected $table = 'proposal';

    protected $primaryKey = 'id_proposal';

    protected $fillable = ['id_user', 'subject', 'proposal', 'budget', 'date_estimate', 'pts_before', 'pts_after', 'id_job', 'id_last_reset', 'created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo('App\Http\Models\User', 'id_user');
    }

    public function job()
    {
        return $this->belongsTo('App\Http\Models\Job', 'id_job');
    }

    /* COUNT PROPOSAL */
    static function countProposal($last)
    {
        return Self::where('id_last_reset', $last)->count();
    }

    /* CHECK UNIQUE JOBS */
    static function uniqueJob($idUser, $idJob)
    {
        return Self::where('id_user', $idUser)->where('id_job', $idJob)->first();
    }

    /* LIST PROPOSAL */
    static function listProposal($post=[]) 
    {
        $proposal = Self::with(['user', 'job']);

        if (isset($post['id_proposal'])) {
            $proposal->where('id_proposal', $post['id_proposal']);
        }

        if (isset($post['id_user'])) {
            $proposal->where('id_user', $post['id_user']);
        }

        if (isset($post['subject'])) {
            $proposal->where('subject', $post['subject']);
        }

        if (isset($post['publish'])) {
            $proposal->where('publish', $post['publish']);
        }

        return $proposal = $proposal->get()->toArray();
    }
}
